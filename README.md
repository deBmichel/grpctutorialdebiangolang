<h1>Fast Grpc Tutorial in Debian using Golang</h1>

<div align="center">
<img src="images/header.png" alt="Header custom image"/>
</div>

It is a <mark>FAST FUNCTIONAL</mark> tutorial about Grpc for Golang developers. **The tutorial should function in all Operating System the only difference should be the installation of Protoc and protoc plugin**.

You are going to find a functional example creating Client and Server for Grpc using golang and a method to install protoc in Debian 10. After this tutorial you should to learn that: protocol buffers are essential for Grpc; play to compile a .proto file to generate the golang contract pb file; have access to a simple functional client/server code for Golang.

<mark>No tecnical concepts, only a functional tuto here. Is important learning about **RPC**, **proto files** **GRPC core** and other important topics as **grpc-gateway**.</mark>

<mark>If you like to go fast or you are looking for **high speed learning (Having previous strong knowledges)** this tutorial has a **FAST EXECUTION SH** for purposes of increasing learning speed and access to functional code **A**s **S**oon **A**s **P**ossible, you must review the [SH Tuto section](#ShTuS) or if you are temperamental and has strong developer experience in linux **I INVITE YOU FIRST TO READ THE SH BEFORE RUN** and execute in your terminal [the tutorial sh](https://gitlab.com/deBmichel/grpctutorialdebiangolang/-/raw/master/FastShGrprctuto.sh) directly.
</mark>

My name is Michel and I hope you enjoy this tutorial and find it functional.

Cheers.

##### Table of Contents:
1. [Installing protoc in debian](#Installingprotocindebian)<br/> * [About protoc](#Aboutprotoc)<br/> * [Installation](#Installation)

2. [Starting with GRPC + golang](#SwGpG)<br/> * [The GOPATH](#TheGOPATH)<br/> * [The First Simple GRPC exercise](#TFSGE)

3. [Sources - Acknowledgments -  Greetings - Thanks](#SaR)

4. [SH Tuto section](#ShTuS)

<a name="Installingprotocindebian"/>

## Installing protoc in debian:

To use and execute GRPC an important step is to install protoc.

<a name="Aboutprotoc"/>

### About protoc:
"""  You use the protocol buffer compiler protoc to generate data access classes in your preferred language(s) from your proto definition. These provide simple accessors for each field, like name() and set_name(), as well as methods to serialize/parse the whole structure to/from raw bytes. So, for instance, if your chosen language is C++, running the compiler on the example above will generate a class called Person. You can then use this class in your application to populate, serialize, and retrieve Person protocol buffer messages.  """  [from the source](https://grpc.io/docs/what-is-grpc/introduction/)

<a name="Installation"/>

### Installation:

1. Verify Debian OS from terminal. You can use the command : <br/> **cat /etc/issue**

![g1:](images/g1.png)

2. Execute the command: <br/> **sudo apt install libprotobuf-dev** <br />

**Libprotobuf-dev is important : [protocol buffers C++ library (development files) and proto files](https://packages.debian.org/sid/libprotobuf-dev). The installation should not have problems.**

![g6:](images/g6.png)

3. Go to (https://github.com/protocolbuffers/protobuf/releases) and download your zip.

![g2:](images/g2.png)

In this case:

![g3:](images/g3.png)

4. Execute the commands: <br/> **sudo unzip -o protoc-3.13.0-linux-x86_64.zip -d /usr/local bin/protoc <br/> sudo unzip -o protoc-3.13.0-linux-x86_64.zip -d /usr/local include/***

![g4:](images/g4.png)

5. Verify the protoc version using the command : <br/> **sudo protoc --version**

![g5:](images/g5.png)

An important thing is execute the command : <br/> **cd /usr/local/bin/ && sudo chmod a+x protoc**

to avoid a classic error begging with GRPC:

<mark>
protoc-gen-go: program not found or is not executable
Please specify a program using absolute path or make sure the program is available in your PATH system variable
--go_out: protoc-gen-go: Plugin failed with status code 1.
</mark> <br /> <br />

![g7:](images/g7.png)

Review this:

![g8:](images/g8.png)

![g9:](images/g9.png)


If you've gotten to this point, the Protoc installation has been completed.

Congratulations.

<a name="SwGpG"/>

## Starting with GRPC + golang:

Once the installation of Protocols in debian is finished, it's time to do a first GRPC exercise using Protocols and Golang.

<a name="TheGOPATH"/>

## The GOPATH:

To play with GRPC is needed to do and export of the GoPath pointing to the bin folder, you can add the next lines <mark>(or similar lines depending on your Golang installation on debian)</mark> in the .bashrc file :

**echo 'adding golang to the terminal .... '**<br/>**export GOROOT=/usr/local/go**<br/>**export GOPATH=$HOME/YOURGoFOLder/**<br/>**export PATH=$GOPATH/bin:$GOROOT/bin:$PATH**

* Why in the .bashrc ?

so that when you open a new terminal the gopath is exported pointing to the bin folder and you can have access to the default Gopath no matter what terminal you have open.

This should avoid headaches related to accessing the GOPATH.

[source](https://grpc.io/docs/languages/go/quickstart/) contains the next step:

... Update your PATH so that the protoc compiler can find the plugin:

**$ export PATH="$PATH:$(go env GOPATH)/bin"**

<a name="TFSGE"/>

## The First Simple GRPC exercise:

The first exercise about GRPC is something simple but functional. <mark>The first exercise is about a Grpc server receiving a request with Name Code and Continent and responding with a md5 hash of the received Name with the data recived.</mark>

+ ##### [Remember: You need to install protoc in debian:](#Installingprotocindebian)

1. Go to your GOPATH folder, and review visually the folder location is equal to: <br /> **go env GOPATH**

and create the folder grpctuto with <br />**mkdir grpctuto**

![g10:](images/g10.png)

2. Go to grpctuto folder and execute the next commands to install needed golang packages:

**go get github.com/golang/protobuf/protoc-gen-go**       <mark>It is a pluggin to generate te golang pb from the **.proto** file</mark> <br />
**go install github.com/golang/protobuf/protoc-gen-go**   <mark>Install protoc-gen-go.</mark> <br />
**go get -u google.golang.org/grpc**                      <mark>The grpc package for Golang</mark> <br />
**go get github.com/pkg/errors**                          <mark>An extra errors manager package for Golang</mark> <br />

![g11:](images/g11.png)

3. Create the file grpctuto.proto in the folder grpctuto and add the next code:

```protobuf

syntax = "proto3";

package grpctuto;

service GrpctutoService {
	rpc Generate(GrpctutoRequest) returns (GrpctutoResponse);
}

message GrpctutoRequest {
	string name = 1;
	uint32 code = 2;
	string continent = 3;
}

message GrpctutoResponse {
	string result = 1;
}

```

4. Compile the proto file using protoc with the command: <br /> **protoc --go_out=plugins=grpc:. \*.proto**

review the image to see before and after compilation and don't worry about the warning, maybe later we'll talk about the go_package tag in the .proto files. <br />

<mark> The important consideration: You shoud see grpctuto.PB.go after compilation</mark>

![g12:](images/g12.png)

5. Create the **folders CLIENT and SERVER** use the commands: <br />**mkdir client** <br />**mkdir server** <br /> and Move all folder project grpctuto to GOPATH/src folder using the command: <br />**mv ../grpctuto/ ../src/** <mark> Is needed to move grpctuto to src to go compile </mark>


![g13:](images/g13.png)

6. Create the file logic.go and add the next code:

```go

package grpctuto

import(
	"crypto/md5"
	"fmt"
)

func NameHash(email string) []byte {
	emailsum := md5.Sum([]byte(email))
	return emailsum[:]
}

func grpcTutoDataProcess(name string, code uint32, continent string) string {
	hashname := NameHash(name)
	return fmt.Sprintf("Line : %s + %d + %s =%x", name, code, continent, hashname)
}

func ManageData(name string, code uint32, continent string) string {
	return grpcTutoDataProcess(name, code,continent)
}

```

![g14:](images/g14.png)

7. In the folder **server** create the file **main.go** and add the next code:

```go

package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	"github.com/pkg/errors"
	
	pb "grpctuto"
)

const port = ":50051"

type grpctutoService struct{}

func (s *grpctutoService) Generate(ctx context.Context, in *pb.GrpctutoRequest) (*pb.GrpctutoResponse, error) {
	log.Printf("Received name %v with code %v and continent %v", in.Name, in.Code, in.Continent)
	return &pb.GrpctutoResponse{Result: pb.ManageData(in.Name, in.Code, in.Continent)}, nil
}

func main() {
	log.Println("Running GRPC server ..... \n")
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Failed to listen on port: %v", err)
	}

	server := grpc.NewServer()
	pb.RegisterGrpctutoServiceServer(server, &grpctutoService{})
	if err := server.Serve(lis); err != nil {
		log.Fatal(errors.Wrap(err, "Failed to start server!"))
	}
}

```

![g15:](images/g15.png)


8. In the folder **client** add the file **main.go** and add the next code:
```go

package main

import (
	"context"
	"time"
	"log"

	"google.golang.org/grpc"
	
	pb "grpctuto"
)

const address = "localhost:50051"

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewGrpctutoServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.Generate(ctx, &pb.GrpctutoRequest{Name: "Michel M S", Code: 1, Continent:"America"})
	if err != nil {
		log.Fatalf("could not execute GrpctutoRequest: %v", err)
	}

	log.Printf("GrpctutoRequest Result: %s", r.Result)
}

```

![g16:](images/g16.png)

<a name="ExItE"/>

9. Test the code: Open two new terminals and use <mark>RESPECTIVE</mark> commands: <br />

In <mark>(A terminal for you)</mark> execute:<br />**cd $GOPATH/src/grpctuto**<br />**go run server/main.go**

In <mark>(B terminal for you)</mark> execute:<br />**cd $GOPATH/src/grpctuto**<br />**go run client/main.go**<br />Wait server answer and **modify manually the line r, err := c.Generate(ctx, &pb.GrpctutoRequest{Name: "Michel M S", Code: 1, Continent:"America"}) in client/main.go** then repeat **go run client/main.go**

![g16:](images/g17.png)

If you've gotten to this point then you have completed the tutorial.

Congratulations and thank you for read this.

<a name="SaR"/>

## Sources - Acknowledgments -  Greetings - Thanks:

To do this tutorial I leveraged information from the following sources:

I thank and invite a lot to thank the creators of this content; because thanks to them now I and those who read this tutorial will have a little idea of GRPC.

I invite those who read this to review the content and creators for the next sources:

* gRPC quickstart (http://www.grpc.io/docs/quickstart/go.html)
* https://blog.lelonek.me/a-brief-introduction-to-grpc-in-go-e66e596fe244
* https://github.com/phuongdo/go-grpc-tutorial
* gRPC advance (http://www.grpc.io/docs/tutorials/basic/go.html#generating-client-and-server-code)
* grpc-gateway a plugin of protoc (https://github.com/grpc-ecosystem/grpc-gateway/)
* https://github.com/grpc/grpc-go/issues/3310
* Images edited with GIMP.
* The image for thank is from : Image from : https://commons.wikimedia.org/wiki/File:Gnu_wallpaper.png

##### Thanks to GNU - Linux - debian: I have learned a lot thanks to the spiral, the gnu and the penguin.
<div align="center">
![Gnu](images/Gnu_wallpaper.png)


****

<a name="ShTuS"/>

## SH Tuto section:

This tutorial has a fast execution SH for purposes of increasing learning speed and access to functional code. **I INVITE YOU TO READ THE SH BEFORE RUN.**

* WARNING: Review the commands before run the SH, and be carefull with the path REEXPORTS.
* Make a copy of your var PATH. Or only copy and paste in terminal commands you need.
* dont forget execute **chmod u+x FastShGrprctuto.sh**
* Review The Image Execution to have a guide and idea

Good sh execution looks like:

[SHEEx:](images/SHEEx.png)

**Note than only server is started with sh, to client review [here](#ExItE) **
</div>












