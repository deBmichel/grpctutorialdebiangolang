#!/bin/bash

# WARNING: Review the commands before run, be carefull
# with the path REEXPORT.
# It is a fast sh to execute all steps of the tutorial : 
# https://gitlab.com/deBmichel/grpctutorialdebiangolang.

echo -e  'Start sh for https://gitlab.com/deBmichel/grpctutorialdebiangolang\n'
echo -e  'assuming YOU HAVE protoc installed: protoc installation no made in this sh....'
echo -e  'WARNING ! : assuming your GOPATH folder for execution ...'
echo -e  'WARNING ! : Reexporting for path ...'
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
cd $GOPATH
echo -e  'WARNING ! : assuming WGET package for debian '
echo -e  'WARNING ! : Be carefull cowboy ... if something fails review:\nprotoc\nGOPATHorwget\n'
# Creating the tutorial folder.
echo -e  'Creating needed dirs'
mkdir grpctuto
cd grpctuto
mkdir client
mkdir server

echo -e  'Installing go packages .... \n'
echo -e  'Installing github.com/golang/protobuf/protoc-gen-go'
go get github.com/golang/protobuf/protoc-gen-go
echo -e  'Installing all github.com/golang/protobuf/protoc-gen-go'
go install github.com/golang/protobuf/protoc-gen-go
echo -e  'Installing -u google.golang.org/grpc'
go get -u google.golang.org/grpc
echo -e  'Installing github.com/pkg/errors'
go get github.com/pkg/errors

echo -e  '\n Adding needed files vía wget: ,,,,,\n'
echo -e  'adding file grpctuto.proto'
wget https://gitlab.com/deBmichel/grpctutorialdebiangolang/-/raw/master/grpctuto/grpctuto.proto
echo -e  'adding file logic.go'
wget https://gitlab.com/deBmichel/grpctutorialdebiangolang/-/raw/master/grpctuto/logic.go
echo -e  'adding client'
wget https://gitlab.com/deBmichel/grpctutorialdebiangolang/-/raw/master/grpctuto/client/main.go
mv main.go client/
echo -e  'adding server'
wget https://gitlab.com/deBmichel/grpctutorialdebiangolang/-/raw/master/grpctuto/server/main.go
mv main.go server/

echo -e  '\ncompiling proto file using the alleged installation of Protoc:'
protoc --go_out=plugins=grpc:. *.proto

echo -e  'Relocating gprc tuto in the src GOPATH folder'
mv ../grpctuto/ ../src/

echo -e  'Only server will be executed the client should be executed in other terminal ...'
cd $GOPATH/src/grpctuto
go run server/main.go

echo -e  'If all was good Congratulation else review:\nprotoc\nGOPATHorwget\n'
echo  'Thanks for review the script'
