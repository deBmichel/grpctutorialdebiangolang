package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	"github.com/pkg/errors"
	
	pb "grpctuto"
)

const port = ":50051"

type grpctutoService struct{}

func (s *grpctutoService) Generate(ctx context.Context, in *pb.GrpctutoRequest) (*pb.GrpctutoResponse, error) {
	log.Printf("Received name %v with code %v and continent %v", in.Name, in.Code, in.Continent)
	return &pb.GrpctutoResponse{Result: pb.ManageData(in.Name, in.Code, in.Continent)}, nil
}

func main() {
	log.Println("Running GRPC server ..... \n")
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Failed to listen on port: %v", err)
	}

	server := grpc.NewServer()
	pb.RegisterGrpctutoServiceServer(server, &grpctutoService{})
	if err := server.Serve(lis); err != nil {
		log.Fatal(errors.Wrap(err, "Failed to start server!"))
	}
}


