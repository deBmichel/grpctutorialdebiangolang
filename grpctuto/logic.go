package grpctuto

import(
	"crypto/md5"
	"fmt"
)

func NameHash(email string) []byte {
	emailsum := md5.Sum([]byte(email))
	return emailsum[:]
}

func grpcTutoDataProcess(name string, code uint32, continent string) string {
	hashname := NameHash(name)
	return fmt.Sprintf("Line : %s + %d + %s =%x", name, code, continent, hashname)
}

func ManageData(name string, code uint32, continent string) string {
	return grpcTutoDataProcess(name, code,continent)
}
