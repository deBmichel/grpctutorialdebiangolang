package main

import (
	"context"
	"time"
	"log"

	"google.golang.org/grpc"
	
	pb "grpctuto"
)

const address = "localhost:50051"

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewGrpctutoServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.Generate(ctx, &pb.GrpctutoRequest{Name: "Golang ", Code: 23, Continent:"África"})
	if err != nil {
		log.Fatalf("could not execute GrpctutoRequest: %v", err)
	}

	log.Printf("GrpctutoRequest Result: %s", r.Result)
}

